package services

import (
	"app/common"
	"crypto/tls"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/joho/godotenv"
)

func init() {
	if len(os.Getenv("BIKES_URL")) == 0 {
		err := godotenv.Load()
		common.CheckOnError(err)
	}
}

var bikeServiceURL = os.Getenv("BIKES_URL") + "/"

// SetBikeUnavailable marks a particular bike as not available (hired)
func SetBikeUnavailable(id string) (string, int, error) {
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}

	req, err := http.NewRequest("PUT", bikeServiceURL+"update/"+id, nil)
	common.CheckOnError(err)

	req.Header.Add("cache-control", "no-cache")
	res, err := http.DefaultClient.Do(req)
	common.CheckOnError(err)

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)

	return string(body), res.StatusCode, err

}

// GetBike retrieves details of a particular bike
func GetBike(id string) (string, int, error) {
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}

	req, err := http.NewRequest("GET", bikeServiceURL+"get/"+id, nil)
	common.CheckOnError(err)

	req.Header.Add("cache-control", "no-cache")
	res, err := http.DefaultClient.Do(req)
	common.CheckOnError(err)

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)

	return string(body), res.StatusCode, err
}
