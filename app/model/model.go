package model

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Agreement :The number of properties needed to setup a new MVP project
type Agreement struct {
	ID           bson.ObjectId `json:"_id,omitempty" bson:"_id,omitempty"`
	CreationTime time.Time     `json:"creation_time,omitempty" bson:"creation_time,omitempty"`
	BikeID       string        `json:"bike,omitempty" binding:"required" bson:",omitempty"`
	CustomerID   string        `json:"customer,omitempty" binding:"required" bson:",omitempty"`
}
