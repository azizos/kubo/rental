package services

import (
	"app/common"
	"crypto/tls"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/joho/godotenv"
)

func init() {
	if len(os.Getenv("CUSTOMERS_URL")) == 0 {
		err := godotenv.Load()
		common.CheckOnError(err)
	}
}

var customerServiceURL = os.Getenv("CUSTOMERS_URL") + "/"

// GetCustomer retrieves details of a particular customer
func GetCustomer(id string) (string, int, error) {
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}

	req, err := http.NewRequest("GET", customerServiceURL+"get/"+id, nil)
	common.CheckOnError(err)

	req.Header.Add("cache-control", "no-cache")
	res, err := http.DefaultClient.Do(req)
	common.CheckOnError(err)

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	return string(body), res.StatusCode, err
}
