package handlers

import (
	"app/common"
	"app/model"
	"app/services"
	"net/http"

	"github.com/gin-gonic/gin"
)

var log, _ = common.GetLogModule()

// CreateAgreement function handles the HTTP requests to create a new agreement in the database
func CreateAgreement(context *gin.Context) {
	var agreement model.Agreement
	if err := context.ShouldBindJSON(&agreement); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	bikeBody, statusCodeBikeReq, err := services.GetBike(agreement.BikeID)
	if statusCodeBikeReq != 200 || err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"message": bikeBody})
		return
	}

	customerBody, statusCodeCustomerReq, err := services.GetCustomer(agreement.CustomerID)
	if statusCodeCustomerReq != 200 || err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"message": customerBody})
		return
	}

	foundID, err := services.CreateAgreement(agreement)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"message": "no agreement could be created in the database"})
		return
	}

	bikeBody, statusCodeBikeReq, err = services.SetBikeUnavailable(agreement.BikeID)
	if statusCodeBikeReq != 200 || err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"message": "bike cannot be updated"})
		return
	}
	log.Info("a new agreement has been addedd:" + foundID)
	context.JSON(http.StatusOK, gin.H{"id": foundID})
	return
}

// DeleteAgreement function handles the HTTP requests to delete an existing agreement in the database
func DeleteAgreement(context *gin.Context) {
	err := services.DeleteAgreement(context.Param("id"))
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"message": "agreement could not be removed"})
	} else {
		context.JSON(http.StatusOK, gin.H{"message": "agreement has been removed"})
	}
}

// // UpdateAgreemen function updates an agreement entity
// func UpdateAgreemen(context *gin.Context) {
// 	agreement, err := services.UpdateAgreement(context.Param("id"))
// 	if err != nil {
// 		context.JSON(http.StatusBadRequest, gin.H{"message": "agreement could not update"})
// 	} else {
// 		context.JSON(http.StatusOK, gin.H{agreement})
// 	}
// }

// GetAgreement function handles the HTTP requests to retrieve agreement objects from the database
func GetAgreement(context *gin.Context) {
	foundAgreement, err := services.GetAgreement(context.Param("id"))
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"message": "no agreement could be found in the database"})
	} else {
		context.JSON(http.StatusOK, foundAgreement)
	}
}

// ListAgreement function handles the HTTP requests to retrieve all agreement objects from the database
func ListAgreement(context *gin.Context) {
	agreements, err := services.ListAgreement()
	if err != nil {
		context.JSON(http.StatusOK, gin.H{"message": "something went wrong, please try again.."})
	} else {
		if len(agreements) <= 0 {
			context.JSON(http.StatusOK, gin.H{"message": "oops, no agreements found :("})
		}
		context.JSON(http.StatusOK, agreements)
	}
}
