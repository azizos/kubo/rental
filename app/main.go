package main

import (
	"app/handlers"

	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	router.POST("/agreements/create", handlers.CreateAgreement)
	router.DELETE("/agreements/delete/:id", handlers.DeleteAgreement)
	// router.PUT("/agreements/update/:id", handlers.UpdateAgreement)
	router.GET("/agreements/get/:id", handlers.GetAgreement)
	router.GET("/agreements/get", handlers.ListAgreement)

	router.Run("0.0.0.0:3002")
}
