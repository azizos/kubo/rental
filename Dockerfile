FROM golang:alpine3.9 as builder

LABEL version="0.1"
LABEL description="Rental service for Paygent"
LABEL maintainer="aziztux[at]gmail"

RUN mkdir /go/src/app && apk update && apk add git
WORKDIR /go/src/app
COPY app .
RUN go get && CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM alpine:3.9
RUN apk update && apk add --no-cache openssl ca-certificates
WORKDIR /home/
EXPOSE 3002
COPY --from=0 go/src/app/app /home/
CMD ["./app","-web.listen-address=0.0.0.0:3002"]
