package services

import (
	"app/common"
	"app/model"
	"os"
	"time"

	"github.com/joho/godotenv"
	"gopkg.in/mgo.v2/bson"
)

func init() {
	if len(os.Getenv("MONGO_USER")) == 0 {
		err := godotenv.Load()
		common.CheckOnError(err)
	}
	if len(os.Getenv("MONGO_PSWD")) == 0 {
		err := godotenv.Load()
		common.CheckOnError(err)
	}
	if len(os.Getenv("MONGO_HOST")) == 0 {
		err := godotenv.Load()
		common.CheckOnError(err)
	}
	common.ConnectDB()
}

// CreateAgreement function adds new agreement objects into the database
func CreateAgreement(agreement model.Agreement) (string, error) {
	var err error
	s := common.Session.Clone()
	defer s.Close()
	agreement.ID = bson.NewObjectId()
	agreement.CreationTime = time.Now()
	err = common.AgreementCollection.Insert(&agreement)
	return agreement.ID.Hex(), err
}

// DeleteAgreement function removes an existing objects from the database
func DeleteAgreement(id string) error {
	_, err := GetAgreement(id)
	if err == nil {
		oID := bson.ObjectIdHex(id)
		err = common.AgreementCollection.Remove(bson.M{"_id": oID})
	}
	return err
}

// // UpdateAgreement function updates an agreement
// func UpdateAgreement(id string) (string, error) {

// }

// GetAgreement retrieves agreement objects from the database
func GetAgreement(id string) (model.Agreement, error) {
	agreement := model.Agreement{}
	oID := bson.ObjectIdHex(id)
	err := common.AgreementCollection.FindId(oID).One(&agreement)
	return agreement, err
}

// ListAgreement function retrieves all agreement objects from the database
func ListAgreement() ([]model.Agreement, error) {
	agreements := []model.Agreement{}
	err := common.AgreementCollection.Find(nil).Sort("-updated_on").All(&agreements)
	return agreements, err
}
