# Rental service

This project is used as a part of the entire Kubo application.
Rental service is responsible to persist and manage all the agreement entities.

# Use

Please find the Postman collection in the [infra](https://gitlab.com/azizoo/kubo/infra) repository.